""" se pide la hipotenusa y el radio"""
hipotenusa = int(input("ingrese la medida de la hipotenusa: "))
radio = int(input("ingrese la medida del radio: "))
"""el cateto faltante será la resta de la hipotenusa y el radio al cuadrado"""
catetofaltante = (hipotenusa **2 - radio **2)**0.5
"""el area de los 2 triangulos se saca de la siguiente manera:"""
areatriangulo = catetofaltante * radio 
areacirculo = (3.14 * radio**2)/ 2
areatotal = areacirculo + areatriangulo

print("el area de la figura es: ", areatotal)